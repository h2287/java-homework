package sample;

import java.util.Arrays;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		int[] arr1 = new int[] { 0, 5, 2, 4, 7, 1, 3, 19 };
		int sum = 0;

		for (int i = 0; i < arr1.length; i++) {
			if (arr1[i] % 2 == 1) {
				sum = sum + 1;
			}
		}
		System.out.println("Кількість непарних чисел: " + sum);
	}

}
