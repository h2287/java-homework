package sample;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Random rn = new Random();
		Scanner sc = new Scanner(System.in);

		int n = 0;

		System.out.println("Enter size of array: ");
		n = sc.nextInt();
		int[] a = new int[n];

		for (int i = 0; i < a.length; i++) {
			a[i] = rn.nextInt(9);
		}
		System.out.println("Arrays before: " + Arrays.toString(a));

		int middleIndex = (n - 1) / 2;

		for (int i = 0; i <= middleIndex; i++) {
			int tempE1 = a[i];
			a[i] = a[n - 1 - i];
			a[n - 1] = tempE1;
		}
		System.out.println("Arrays after: " + Arrays.toString(a));
	}

}
