package sample;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		int[][] a = new int[][] {
				{ 1, 5, 1, 2, 4, 4, 1, 7 }, 
				{ 5, 1, 1, 4, 6, 9, 1, 3 }, 
				{ 8, 8, 4, 1, 9, 2, 3, 3 },
				{ 2, 6, 8, 7, 6, 0, 6, 9 }, 
				{ 2, 1, 5, 4, 0, 5, 6, 3 }, 
				{ 7, 1, 4, 9, 6, 3, 8, 9 },
				{ 9, 0, 2, 4, 9, 7, 4, 6 }, 
				{ 6, 3, 7, 7, 9, 3, 8, 9 }

		};

		System.out.println("Origin array: ");
		for (int i = 0; i < a.length; i++) {
			System.out.println(Arrays.toString(a[i]));
		}
		System.out.println();

		for (int i = 1; i <= 3; i++) {
			for (int j = 0; j < a.length / 2; j++) {
				for (int k = 0; k < a.length; k++) {
					int tempE1 = a[j][k];
					a[j][k] = a[a.length - 1 - j][k];
					a[a.length - 1 - j][k] = tempE1;
				}
			}
			for (int j = 0; j < a.length; j++) {
				for (int k = j; k < a.length; k++) {
					int tempE1 = a[k][j];
					a[k][j] = a[j][k];
					a[j][k] = tempE1;

				}
			}
			System.out.println("Array rotated " + 90 * i + " degrees: ");
			for (int j = 0; j < a.length; j++) {
				System.out.println(Arrays.toString(a[j]));
			}
			System.out.println();
		}
	}

}
