package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double x;
		double y;

		System.out.println("Input value x:");
		x = sc.nextDouble();
		System.out.println("Input value y:");
		y = sc.nextDouble();

		if ((0 - x) * (4 - 0) - (4 - 0) * (0 - y) >= 0 && (4 - x) * (1 - 4) - (6 - 4) * (4 - y) >= 0
				&& (6 - x) * (0 - 1) - (0 - 6) * (1 - y) >= 0
				|| (0 - x) * (4 - 0) - (4 - 0) * (0 - y) <= 0 && (4 - x) * (1 - 4) - (6 - 4) * (4 - y) <= 0
						&& (6 - x) * (0 - 1) - (0 - 6) * (1 - y) <= 0) {

			System.out.println("Точка лежит внутри треугольника");
		} else {
			System.out.println("Точка  не лежит внутри треугольника");
		}
	}

}
