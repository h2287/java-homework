package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numberFlat;

		System.out.println("Input flat number.");
		numberFlat = sc.nextInt();

		int entrance = (numberFlat - 1) / 36 + 1;
		int flatFloor = (numberFlat - 36 * (entrance - 1) - 1) / 4 + 1;
		if (numberFlat > 0 && numberFlat <= 180) {
			System.out.println("Flat number: " + numberFlat);
			System.out.println("Entrance: " + entrance);
			System.out.println("Floor: " + flatFloor);
		} else {
			System.out.println("Flat isn't find!");
		}

	}

}
