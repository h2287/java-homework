package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int a;
		int b;
		int c;

		System.out.println("Input length a:");
		a = sc.nextInt();

		System.out.println("Input length b:");
		b = sc.nextInt();

		System.out.println("Input length c:");
		c = sc.nextInt();

		if (((a + b) > c) && ((b + c) > a) && ((c + a) > b)) {
			System.out.println("Triangle exists :)");
		} else {
			System.out.println("Triangle don't exists :(");
		}

	}

}
