package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int numb = sc.nextInt();
		int a = numb / 100000;
		int b = numb % 100000 / 10000;
		int c = numb % 100000 % 10000 / 1000;
		int d = numb % 100000 % 10000 % 1000 / 100;
		int e = numb % 100000 % 10000 % 1000 % 100 / 10;
		int f = numb % 100000 % 10000 % 1000 % 100 % 10;

		if (a == f && b == e && c == d) {
			System.out.println("Вітаю! Це число є Паліндромом.");
		} else {
			System.out.println("Ви помилися. Це чисо не є Паліндромом.");
		}
	}

}
