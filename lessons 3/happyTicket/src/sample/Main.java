package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int numb = sc.nextInt();
		int a = numb / 1000;
		int b = numb % 1000 / 100;
		int c = numb % 1000 % 100 / 10;
		int d = numb % 1000 % 100 % 10;

		int sum1 = a + b;
		int sum2 = c + d;

		if (sum1 == sum2) {
			System.out.println("Сщасливий білет :)!");
		} else {
			System.out.println("Не пощастило :(");
		}
	}

}
