package sample;

public class Main {

	public static void main(String[] args) {
		double priceFuel = 1.2;
		double distance = 120;
		double spendFuel = 8;

		double priceRoad = distance * priceFuel * spendFuel / 100;
		System.out.println("Вартість палива на дану поїздку: " + priceRoad + "$");
	}

}
