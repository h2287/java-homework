package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int size = 0;
		String text = "";
		String name = "";

		System.out.print("Enter file name: ");
		name = in.nextLine();

		System.out.print("Enter lines size: ");
		size = in.nextInt();

		File file = new File(name + ".txt");
		saveTextToFile(file, text, size);
	}

	public static void saveTextToFile(File file, String text, int size) {
		Scanner in = new Scanner(System.in);

		try (PrintWriter pw = new PrintWriter(file)) {
			for (int i = 0; i < size; i++) {
				System.out.print("Enter line text " + i + " : ");
				text = in.nextLine();
				pw.println(text);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}