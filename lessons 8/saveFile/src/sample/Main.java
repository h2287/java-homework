package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Main {

	public static int[][] randomArray(int twoDimArray[][]) {
		Random rn = new Random();
		for (int i = 0; i < twoDimArray.length; i++) {
			for (int j = 0; j < twoDimArray.length; j++) {
				twoDimArray[i][j] = rn.nextInt(123);
			}
		}
		return twoDimArray;
	}

	public static void showArray(int[][] twoDimArray, File file) {
		try (PrintWriter pw = new PrintWriter(file)) {
			for (int i = 0; i < twoDimArray.length; i++) {
				for (int j = 0; j < twoDimArray.length; j++) {
					pw.print(" " + twoDimArray[i][j] + " ");
				}
				pw.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int[][] twoDimArray = new int[5][5];
		File file = new File("randomNumbers.txt");

		randomArray(twoDimArray);
		showArray(twoDimArray, file);

	}

}
