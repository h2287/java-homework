package sample;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Random rn = new Random();

		int[] array = new int[15];
		for (int i = 0; i < array.length; i++) {
			array[i] = rn.nextInt(100);
		}
		
		System.out.println(Arrays.toString(array));

		System.out.println("Enter number to find: ");
		int Number = sc.nextInt();
		System.out.println(search(array, Number));
	}

	public static int search(int[] arr, int n) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == n) {
				return i;
			}
		}
		return -1;
	}

}
