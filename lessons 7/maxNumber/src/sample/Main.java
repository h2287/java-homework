package sample;

public class Main {

	public static void main(String[] args) {
		int[] arr1 = new int[] { 14, -6, 27, 1, 6, 13 };
		System.out.println(getMax(arr1));
		System.out.println(getMin(arr1));
	}

	public static int getMax(int[] arr1) {
		int currentMax = arr1[0];
		for (int i = 0; i < arr1.length; i++) {
			if (arr1[i] > currentMax) {
				currentMax = arr1[i];
			}
		}
		return currentMax;
	}

	public static int getMin(int[] arr1) {
		int currentMin = arr1[0];
		for (int i = 0; i < arr1.length; i++) {
			if (arr1[i] < currentMin) {
				currentMin = arr1[i];
			}
		}
		return currentMin;
	}
}
