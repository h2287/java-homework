package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int h;
		int l;

		System.out.println("Enter h: ");
		h = sc.nextInt();
		System.out.println("Enter l: ");
		l = sc.nextInt();

		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= l; j++) {
				if (i == 1 || i == h || j == 1 || j == l) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}

	}

}
