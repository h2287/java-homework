package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n;
		long factorial;

		System.out.println("Enter number from 5-15: ");
		n = sc.nextInt();

		if (n > 4 && n < 16) {
			factorial = 1;

			for (int i = 1; i <= n; i++) {
				factorial = factorial * i;
			}
			System.out.println(n + "! = " + factorial);
		} else {
			System.out.println("Incorect number. Try again!");
		}
	}

}
