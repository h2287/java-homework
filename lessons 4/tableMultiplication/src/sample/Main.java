package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n;
		int result;

		System.out.println("Input number: ");
		n = sc.nextInt();

		for (int i = 1; i <= 10; i++) {
			result = n * i;
			System.out.println(n + " * " + i + " = " + result);
		}
	}

}
