package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double value;
		double prec;
		double modulus;
		double x0;
		double x1;

		System.out.println("Enter a true number: ");
		value = sc.nextDouble();
		System.out.println("Enter the precision: ");
		prec = sc.nextDouble();

		x0 = value;
		x1 = 1 / 2.0 * (x0 + value / x0);
		modulus = x1 - x0;
		if (modulus < 0.0) {
			modulus = modulus * -1.0;
		}

		while ((modulus >= 2 * prec) && (modulus * modulus >= 2 * prec)) {
			x0 = x1;
			x1 = 1 / 2.0 * (x0 + value / x0);

			modulus = x1 - x0;
			if (modulus < 0.0) {
				modulus = modulus * -1.0;
			}
		}
		System.out.println("Real square root of " + value + " with " + prec + " precision is " + x1);
	}

}
