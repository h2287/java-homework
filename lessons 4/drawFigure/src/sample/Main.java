package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input number: ");
		int n = sc.nextInt();
		int b = 1;

		for (int i = 1; i <= n; i++) {
			System.out.print("*");
			if (b <= i && b < n) {
				System.out.println();
				b++;
				i = 0;
			}
			if (n == i) {
				System.out.println();
				n--;
				i = 0;
			}
		}
	}

}
