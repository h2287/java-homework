package sample;

public class Main {

	public static void main(String[] args) {
		for (int i = 2; i <= 11; i++) {
			String text = String.format("%." + i + "f", Math.PI);
			System.out.println("PI: " + text);
		}
	}

}
