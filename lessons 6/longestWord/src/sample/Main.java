package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Input word: ");
		String word = sc.nextLine();

		String[] wordArr = word.split("[ ]");
		int index = 0;

		for (int i = 0; i < wordArr.length; i++) {
            if(wordArr[i].length()> wordArr[index].length()) {
                index = i;
            }
        }
		System.out.println("Longest word: " + wordArr[index]);
	}

}

