package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Input word: ");
		String word = sc.nextLine();
		int number = 1;
		int sum = 0;

		char[] letter = word.toCharArray();
		for (int i = 0; i < letter.length; i++) {
			if (letter[i] == 'b') {
				sum += number;
			}
		}
		System.out.println("Кількість букв (b): " + sum);
	}

	
}
